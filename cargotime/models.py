from django.db import models

# Create your models here.
class CargoStartStop(models.Model):
    start_time = models.DateTimeField()
    stop_time = models.DateTimeField()
    work_time = models.TimeField()
    idle_time = models.TimeField(blank=True, null=True)