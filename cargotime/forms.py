from django import forms
import datetime


class CargoTimeForm(forms.Form):
    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    start_time = forms.DateTimeField(widget=forms.TimeInput(format='%Y-%m-%d %H:%M'), initial=now)     
    stop_time = forms.DateTimeField(widget=forms.TimeInput(format='%Y-%m-%d %H:%M'), initial=now)

 