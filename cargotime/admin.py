from django.contrib import admin

# Register your models here.
from .models import CargoStartStop

admin.site.register(CargoStartStop)