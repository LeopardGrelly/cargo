from django.urls import path
from django.contrib.auth import logout
from . import views

urlpatterns = [
    path('cleardata/', views.clear_data, name='cleardata'),
    path('calc/', views.calc, name='calc'),
]

