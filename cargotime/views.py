from django.shortcuts import render, redirect
from django.http import HttpResponse
from datetime import datetime, date

from .models import CargoStartStop
from .forms import CargoTimeForm


def calc_idle():
    objs = CargoStartStop.objects.all()
    for i in range(len(objs)-1):
        objs[i].idle_time = str(objs[i+1].start_time - objs[i].stop_time)
        objs[i].save()

def clear_data(request):
    CargoStartStop.objects.all().delete()
    return redirect(calc)

def calc(request):
    calc_idle()
    start_stop_objs = CargoStartStop.objects.all()
    if request.method == 'POST':
        form = CargoTimeForm(request.POST)
 
        if form.is_valid():
            
            start = form.cleaned_data['start_time']
            stop = form.cleaned_data['stop_time']
            
            work_time = stop - start          

            obj = CargoStartStop(start_time=start, stop_time=stop, work_time=str(work_time))
            obj.save()

    else:
        form = CargoTimeForm()

    context = {'form': form, 'start_stop_objs': start_stop_objs}
    return render(request, 'cargotime/calc.html', context)
